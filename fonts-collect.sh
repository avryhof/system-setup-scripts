#!/bin/bash
if [ ! -d "~/fonts/truetype" ]; then
  echo Creating ~/fonts/truetype
  mkdir ~/fonts/truetype
fi
if [ ! -d "~/fonts/opentype" ]; then
  echo Creating ~/fonts/opentype
  mkdir ~/fonts/opentype
fi
if [ ! $# -eq 0 ]; then
  if [ ! -d "~/fonts/truetype/$1" ]; then
    echo Creating ~/fonts/truetype/$1
    mkdir ~/fonts/truetype/$1
  fi
  if [ ! -d "~/fonts/opentype/$1" ]; then
    echo Creating ~/fonts/opentype/$1
    mkdir ~/fonts/opentype/$1
  fi
fi
echo Collecting Fonts
if [ ! $# -eq 0 ]; then
  echo TTF
  find . -name "*.ttf" -exec mv "{}" ~/fonts/truetype/$1 \;
  find . -name "*.TTF" -exec mv "{}" ~/fonts/truetype/$1 \;
  echo OTF
  find . -name "*.otf" -exec mv "{}" ~/fonts/opentype/$1 \;
  find . -name "*.OTF" -exec mv "{}" ~/fonts/opentype/$1 \;
fi
if [ $# -eq 0 ]; then
  echo TTF
  find . -name "*.ttf" -exec mv "{}" ~/fonts/truetype \;
  find . -name "*.TTF" -exec mv "{}" ~/fonts/opentype/$1 \;
  echo OTF
  find . -name "*.otf" -exec mv "{}" ~/fonts/opentype \;
  find . -name "*.OTF" -exec mv "{}" ~/fonts/opentype/$1 \;
fi

