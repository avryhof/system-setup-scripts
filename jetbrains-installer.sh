#!/bin/bash
sudo add-apt-repository ppa:webupd8team/java --remove --yes
sudo add-apt-repository ppa:mystic-mirage/pycharm --remove --yes
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make --remove --yes
sudo add-apt-repository ppa:webupd8team/java --yes
sudo add-apt-repository ppa:mystic-mirage/pycharm --yes
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make --yes
sudo apt-get update
sudo apt-get install oracle-java8-installer oracle-java8-set-default pycharm ubuntu-make
umake ide phpstorm
