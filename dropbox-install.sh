#!/bin/bash
sudo apt-get remove nautilus-dropbox
wget https://www.dropbox.com/download?dl=packages/ubuntu/dropbox_2015.10.28_amd64.deb --output-file=dropbox_2015.10.28_amd64.deb
sudo dpkg -i dropbox_2015.10.28_amd64.deb
sudo apt-get install nemo-dropbox
gsettings set org.gnome.desktop.background show-desktop-icons false
