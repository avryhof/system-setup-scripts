#!/bin/bash
echo Purging PPAs
sudo add-apt-repository ppa:embrosyn/cinnamon --remove --yes
sudo add-apt-repository ppa:kranich/cubuntu --remove --yes
sudo add-apt-repository ppa:no1wantdthisname/ppa --remove --yes
sudo add-apt-repository ppa:libreoffice/ppa --remove --yes
sudo add-apt-repository ppa:libreoffice/libreoffice-5-1 --remove --yes
sudo add-apt-repository ppa:mystic-mirage/pycharm --remove --yes
sudo add-apt-repository ppa:cordova-ubuntu/ppa --remove --yes
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make --remove --yes
sudo add-apt-repository ppa:otto-kesselgulasch/gimp --remove --yes 
sudo add-apt-repository ppa:nilarimogard/webupd8 --remove --yes
sudo add-apt-repository ppa:webupd8team/java --remove --yes

sudo add-apt-repository ppa:embrosyn/cinnamon --yes
sudo add-apt-repository ppa:kranich/cubuntu --yes
sudo add-apt-repository ppa:no1wantdthisname/ppa --yes
sudo add-apt-repository ppa:libreoffice/ppa --yes
sudo add-apt-repository ppa:libreoffice/libreoffice-5-1 --yes
sudo add-apt-repository ppa:mystic-mirage/pycharm --yes
sudo add-apt-repository ppa:cordova-ubuntu/ppa --yes
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make --yes
sudo add-apt-repository ppa:otto-kesselgulasch/gimp --yes 
sudo add-apt-repository ppa:nilarimogard/webupd8 --yes
sudo add-apt-repository ppa:webupd8team/java --yes

wget https://archive.getdeb.net/install_deb/getdeb-repository_0.1-1~getdeb1_all.deb
sudo dpkg -i getdeb-repository_0.1-1~getdeb1_all.deb

if [ ! $1 -eq "32" ]; then
  wget https://download.vivaldi.com/stable/vivaldi-stable_1.0.435.42-1_amd64.deb
  sudo dpkg -i vivaldi-stable_1.0.435.42-1_amd64.deb
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo dpkg -i google-chrome*.deb
fi

if [ $1 -eq "32" ]; then
  wget https://download.vivaldi.com/stable/vivaldi-stable_1.0.435.42-1_i386.deb
  sudo dpkg -i vivaldi-stable_1.0.435.42-1_i386.deb
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_i386.deb
  sudo dpkg -i google-chrome*.deb
fi

sudo apt-get update
