#!/bin/bash
echo Adding repository and installing Google Drive FUSE Filesystem
sudo add-apt-repository ppa:alessandro-strada/ppa --remove --yes
sudo add-apt-repository ppa:alessandro-strada/ppa --yes
sudo apt-get update
sudo apt-get install google-drive-ocamlfuse
echo Authorizing
google-drive-ocamlfuse
