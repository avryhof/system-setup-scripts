#!/bin/bash
echo Copying collected fonts to system fonts folder
sudo cp -r ~/fonts /usr/share/fonts
echo Rebuilding font cache.  This could take some time.
sudo fc-cache -f -v
