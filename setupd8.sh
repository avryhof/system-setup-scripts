#!/bin/bash
add-apt-repository ppa:nilarimogard/webupd8 --remove --yes
add-apt-repository ppa:webupd8team/atom --remove --yes
add-apt-repository ppa:webupd8team/atraci --remove --yes
add-apt-repository ppa:webupd8team/brackets --remove --yes
add-apt-repository ppa:webupd8team/java --remove --yes
add-apt-repository ppa:webupd8team/sublime-text-2 --remove --yes
add-apt-repository ppa:webupd8team/sublime-text-3 --remove --yes
add-apt-repository ppa:webupd8team/themes --remove --yes
add-apt-repository ppa:webupd8team/y-ppa-manager --remove --yes

add-apt-repository ppa:nilarimogard/webupd8 --yes
add-apt-repository ppa:webupd8team/atom --yes
add-apt-repository ppa:webupd8team/atraci --yes
add-apt-repository ppa:webupd8team/brackets --yes
add-apt-repository ppa:webupd8team/java --yes
add-apt-repository ppa:webupd8team/sublime-text-2 --yes
add-apt-repository ppa:webupd8team/sublime-text-3 --yes
add-apt-repository ppa:webupd8team/themes --yes
add-apt-repository ppa:webupd8team/y-ppa-manager --yes
