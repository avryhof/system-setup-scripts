System Config Scripts
===================

Cinnamon Desktop
-
- cinnamon-install.sh - Install the latest Cinnamon Release and Numix themes
- dropbox-install.sh - Install nemo-dropbox.
- numix-get-wallpapers.sh - Downloads all of the Free Wallpapers from the Numix Project, and puts them in the system background folder.
- synaptic-install.sh - Install Synaptic and fix the quick filter.

File Sync Services
-
- google-setup.sh - A Google Drive OCAML Fuse connector. Makes Google Drive appear to your system as a mounted network drive.
- google-mount.sh - Use this in combination with Startup programs to automatically mount your Google Drive.  (Easier than adding to fstab)
- onedrive-setup.sh - This is experimental, and will install a onedrive sync client...but it's mostly commandline.
- dropbox-install.sh - Removes everything preventing nemo-dropbox from installing, then installs it correctly.

Fonts
-
- fonts-collect.sh - Collect all fonts in the current folder and below in your home directory and prepares them for easy system-wide installation in Ubuntu.
- fonts-install-collected.sh - Installs fonts collected by the above script from your home directory
- fonts-install-fromhere.sh - If you put the font collection folder from your home directory somewhere else, place this script in that folder and run it.
- fonts-install-corefonts.sh - Installs the Microsoft fonts.  

Handy PPAs
-
- setupd8.sh - Add several WebUpd8 repositories with handy stuff for programming.
- ppa-setup.sh - Adds several PPAs that provide packages newer than the main distro ones. Very heavily slanted toward Cinnamon.
- ppa-disable.sh - Disables PPAs for an upgrade
- ppa-enable.sh - Re-enable PPAs after a dist-upgrade

Handy Extra Software
-
- jetbrains-installer.sh - Installs PyCharm and Phpstorm using Repos and Ubuntu Make.  Makes it easier to keep them up to date.
- skype-web-installer.sh - A script that puts Skype Web into a nice desktop-like package.  Better than the current Skype Linux Client
- process-monitor.sh - A script to put in your home directory and run software with.  This will restart the program when it is closed or crashes. Handy for pre-release stuff.

