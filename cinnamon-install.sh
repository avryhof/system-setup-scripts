#!/bin/bash
echo Removing PPAs that may already exist
sudo add-apt-repository ppa:embrosyn/cinnamon --remove --yes
sudo add-apt-repository ppa:kranich/cubuntu --remove --yes
sudo add-apt-repository ppa:numix/ppa --remove --yes
echo Adding needed PPAs
sudo add-apt-repository ppa:embrosyn/cinnamon --yes
sudo add-apt-repository ppa:kranich/cubuntu --yes
sudo add-apt-repository ppa:numix/ppa --yes
echo Updating and installing
sudo apt-get update && sudo apt-get install cinnamon blueberry numix-gtk-theme numix-icon-theme numix-icon-theme-circle
echo Fixing Shutdown window
gsettings set org.cinnamon.desktop.session settings-daemon-uses-logind true
gsettings set org.cinnamon.desktop.session session-manager-uses-logind true
gsettings set org.cinnamon.desktop.session screensaver-uses-logind false
gsettings set org.gnome.desktop.background show-desktop-icons false
