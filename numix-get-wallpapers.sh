#!/bin/bash
echo Downloading Wallpapers
mkdir ~/numix-wallpapers
cd ~/numix-wallpapers
wget http://orig10.deviantart.net/a7ff/f/2013/224/b/6/numix___name_of_the_doctor___wallpaper_by_satya164-d6hvzh7.zip
wget http://orig08.deviantart.net/4842/f/2013/305/3/6/numix___halloween___wallpaper_by_satya164-d6skv0g.zip
wget http://orig05.deviantart.net/8863/f/2013/249/7/6/numix___fragmented_space_by_me4oslav-d6l8ihd.zip
wget http://orig07.deviantart.net/5ed5/f/2014/143/8/9/numix_wallpapers___aurora_by_paolorotolo-d7je57f.zip
find . -name "*.zip" -exec unzip "{}" \;
find . -name "*.png" -exec mv "{}" ~/numix-wallpapers \;
sudo cp *.png /usr/share/backgrounds/
cd -
rm -fr ~/numix-wallpapers
