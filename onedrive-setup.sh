#!/bin/bash
if [ ! -d "~/OneDriveClient" ]; then
  mkdir ~/OneDriveClient
fi
cd ~/OneDriveClient
wget https://github.com/xybu/onedrive-d/archive/master.zip
unzip master.zip
mv onedrive-d-master/* ./
sudo apt-get update
sudo apt-get install python3 python3-pip
sudo pip install --upgrade pip
sudo pip3 install --upgrade setuptools
sudo python3 setup.py build
sudo python3 setup.py install

