#!/bin/bash
if [ ! -d "~/GoogleDrive" ]; then
  echo Creating ~/GoogleDrive
  mkdir ~/GoogleDrive
fi
/usr/bin/google-drive-ocamlfuse ~/GoogleDrive
